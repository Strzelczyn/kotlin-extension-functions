fun String.lastSign(): Char {
    return this[this.length - 1]
}

operator fun String.div(HowManyTimes: Int): String {
    var text: String = ""
    for (i in 0 until HowManyTimes) {
        text += this.repeat(1)
        if(i+1<HowManyTimes) {
            text += "/"
        }
    }
    return text
}